export_settings
===============

Drupal Export Settings module

* Developed by Stillfinder.
* http://stillfinder.net
* https://github.com/stillfinder/export_settings

This module can be used for export settings of Drupal site to
another environment. This module enables you to select which settings of site you want to export.

Installation
------------
1. Download latest module code.
2. Enable module.


Usage
-----
1. Goto admin/config/system/export_settings url.
2. Select settings for export.
3. Enter name of module with settings.
4. Push Export Settings button.


